import React, { useEffect, useRef } from 'react'
import classes from './card.module.css'
import useOnScreen from '../hooks/onScreen'


const Card = ( props ) => {
    const { id, active, activateCard } = props
    const ref = useRef();
    const [setRef, visible] = useOnScreen( { threshold: 0.5, rootMargin: '0px' } );
    useEffect( () => {
        if ( visible ) {
            props.activate( id )
        }
    }, [visible] )

    return (
        <div ref={setRef} className={active ? classes.active : classes.inactive}></div>
    )
}

export default Card