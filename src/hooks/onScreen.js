import { useEffect, useState } from 'react'
//options: root (photocards container), rootMargin, threshold(like 30% for nice animations)
//observer.observe(target) whenever target hits threshold for intersectionObserver callback is called
const useOnScreen = ( options ) => {
    const [visible, setVisible] = useState( false )
    const [ref, setRef] = useState( null );
    useEffect( () => {
        //callback called on crossing threshold is executed
        const observer = new IntersectionObserver( ( [entry] ) => {
            setVisible( entry.isIntersecting )
        }, options )
        if ( ref ) {
            observer.observe( ref )
        }
        // whenever variables at bottom changed we return this clean up and remove the ref
        return () => {
            if ( ref ) {
                observer.unobserve( ref )
            }
        }
    }, [ref, options] )

    return [setRef, visible]
}

export default useOnScreen