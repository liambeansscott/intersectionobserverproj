import React, { useState } from 'react';
import './App.css';
import useOnScreen from './hooks/onScreen'
import Card from './components/card'
// import classes from './App.module.css';

function App() {
  const [activeCard, setActiveCard] = useState( 0 )

  const handleActivate = ( id ) => setActiveCard( id )

  const cards = () => {
    const array = []
    for ( let i = 0; i < 10; i++ ) {
      array.push(
        <Card id={i} active={i === activeCard} activate={handleActivate} />
      )
    }
    return array
  }

  return (
    <div className="App">
      {cards()}
    </div>
  );
}

export default App;
